module BoardWithoutESP() {
    translate([-158, 150]) {
        #import("Board.stl");
    }
    translate([7.7, 7.7, 3]) {
        rotate([0, 0, -90]) {
            include <Button.scad>;
        }
    }
    translate([21.8, 7.7, 3]) {
        rotate([0, 0, -90]) {
            include <Button.scad>;
        }
    }
    translate([-4.6, 30, 1.6]) {
        rotate([90, 0, -90]) {
            import("Connector.stl");
        }
    }
}
BoardWithoutESP();