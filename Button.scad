// Units: mm.
$fn = 200;

union() {
    difference() {
        cylinder(d = 12, h = 5.75);
        translate([5.5, -10, -5]) {
            cube([0.5, 20, 20]);
        }
    }
    translate([0, 0, 7.25 / 2]) {
        cylinder(d = 8.8, h = 7.25);
    }
    translate([0, 0, -1.4]) {
        cylinder(d = 3.22, h = 1.4);
    }
    for (a = [-1, 1]) {
        for (b = [-1, 1]) {
            translate([a * 5 / 2, b * 5 / 2, -5]) {
                cylinder(d = 0.8, h = 5);
            }
        }
    }
}