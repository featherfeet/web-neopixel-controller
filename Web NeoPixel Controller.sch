EESchema Schematic File Version 4
LIBS:Web NeoPixel Controller-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_RF-Transceiver-Modules:2491 MOD1
U 1 1 5B3C55BE
P 6700 3600
F 0 "MOD1" H 6412 3945 60  0000 C CNN
F 1 "2491" V 6600 3300 60  0000 C CNN
F 2 "digikey-footprints:WIFI_Module_ESP8266_2491" H 6900 3800 60  0001 L CNN
F 3 "https://www.adafruit.com/images/product-files/2471/0A-ESP8266__Datasheet__EN_v4.3.pdf" H 6900 3900 60  0001 L CNN
F 4 "1528-1438-ND" H 6900 4000 60  0001 L CNN "Digi-Key_PN"
F 5 "2491" H 6900 4100 60  0001 L CNN "MPN"
F 6 "RF/IF and RFID" H 6900 4200 60  0001 L CNN "Category"
F 7 "RF Transceiver Modules" H 6900 4300 60  0001 L CNN "Family"
F 8 "https://www.adafruit.com/images/product-files/2471/0A-ESP8266__Datasheet__EN_v4.3.pdf" H 6900 4400 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/adafruit-industries-llc/2491/1528-1438-ND/5761206" H 6900 4500 60  0001 L CNN "DK_Detail_Page"
F 10 "ESP8266 SMT MODULE" H 6900 4600 60  0001 L CNN "Description"
F 11 "Adafruit Industries LLC" H 6900 4700 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6900 4800 60  0001 L CNN "Status"
	1    6700 3600
	1    0    0    -1  
$EndComp
$Comp
L Web-NeoPixel-Controller-rescue:CONN_01X03 P1
U 1 1 5B3C57E3
P 4800 800
F 0 "P1" H 4800 1000 50  0000 C CNN
F 1 "CONN_01X03" V 4900 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 4800 800 50  0001 C CNN
F 3 "" H 4800 800 50  0000 C CNN
	1    4800 800 
	0    -1   -1   0   
$EndComp
$Comp
L dk_PMIC-Voltage-Regulators-Linear:AZ1117CH-3_3TRG1 U1
U 1 1 5B3EC136
P 4450 3250
F 0 "U1" H 4500 3400 60  0000 C CNN
F 1 "AZ1117CH-3_3TRG1" H 4500 3500 60  0000 C CNN
F 2 "digikey-footprints:SOT-223" H 4650 3450 60  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117C.pdf" H 4650 3550 60  0001 L CNN
F 4 "AZ1117CH-3.3TRG1DICT-ND" H 4650 3650 60  0001 L CNN "Digi-Key_PN"
F 5 "AZ1117CH-3.3TRG1" H 4650 3750 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4650 3850 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 4650 3950 60  0001 L CNN "Family"
F 8 "https://www.diodes.com/assets/Datasheets/AZ1117C.pdf" H 4650 4050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/diodes-incorporated/AZ1117CH-3.3TRG1/AZ1117CH-3.3TRG1DICT-ND/4505206" H 4650 4150 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 800MA SOT223" H 4650 4250 60  0001 L CNN "Description"
F 11 "Diodes Incorporated" H 4650 4350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4650 4450 60  0001 L CNN "Status"
	1    4450 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3050 3900 3650
Wire Wire Line
	3900 3650 3950 3650
$Comp
L Web-NeoPixel-Controller-rescue:C C1
U 1 1 5B3EF973
P 4050 3450
F 0 "C1" H 4075 3550 50  0000 L CNN
F 1 "10uF" H 4075 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4088 3300 50  0001 C CNN
F 3 "" H 4050 3450 50  0000 C CNN
	1    4050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3600 4050 3650
Connection ~ 4050 3650
Wire Wire Line
	4850 3250 4950 3250
Wire Wire Line
	5250 3250 5250 3100
Wire Wire Line
	5250 3100 6650 3100
$Comp
L Web-NeoPixel-Controller-rescue:C C2
U 1 1 5B3EFCE8
P 4950 3450
F 0 "C2" H 4975 3550 50  0000 L CNN
F 1 "22uF" H 4975 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 3300 50  0001 C CNN
F 3 "" H 4950 3450 50  0000 C CNN
	1    4950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3250 4950 3300
Connection ~ 4950 3250
Wire Wire Line
	4950 3600 4950 3700
Wire Wire Line
	4950 3700 4250 3700
Wire Wire Line
	4250 3700 4250 3650
Connection ~ 4250 3650
Text Label 4900 1000 0    60   ~ 0
GND
$Comp
L dk_Pushbutton-Switches:KS-01Q-01 S1
U 1 1 5B3FDAA1
P 5550 2650
F 0 "S1" H 5550 2850 50  0000 C CNN
F 1 "KS-01Q-01" H 5550 2475 50  0000 C CNN
F 2 "digikey-footprints:PushButton_Round_D12mm_THT_KS-01Q-01" H 5750 2850 50  0001 L CNN
F 3 "http://spec_sheets.e-switch.com/specs/29-KS01Q01.pdf" H 5750 2950 60  0001 L CNN
F 4 "EG4791-ND" H 5750 3050 60  0001 L CNN "Digi-Key_PN"
F 5 "KS-01Q-01" H 5750 3150 60  0001 L CNN "MPN"
F 6 "Switches" H 5750 3250 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H 5750 3350 60  0001 L CNN "Family"
F 8 "http://spec_sheets.e-switch.com/specs/29-KS01Q01.pdf" H 5750 3450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/e-switch/KS-01Q-01/EG4791-ND/2116270" H 5750 3550 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSH SPST-NO 0.01A 35V" H 5750 3650 60  0001 L CNN "Description"
F 11 "E-Switch" H 5750 3750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5750 3850 60  0001 L CNN "Status"
	1    5550 2650
	1    0    0    -1  
$EndComp
$Comp
L dk_Pushbutton-Switches:KS-01Q-01 S2
U 1 1 5B3FDB47
P 6500 2650
F 0 "S2" H 6500 2850 50  0000 C CNN
F 1 "KS-01Q-01" H 6500 2475 50  0000 C CNN
F 2 "digikey-footprints:PushButton_Round_D12mm_THT_KS-01Q-01" H 6700 2850 50  0001 L CNN
F 3 "http://spec_sheets.e-switch.com/specs/29-KS01Q01.pdf" H 6700 2950 60  0001 L CNN
F 4 "EG4791-ND" H 6700 3050 60  0001 L CNN "Digi-Key_PN"
F 5 "KS-01Q-01" H 6700 3150 60  0001 L CNN "MPN"
F 6 "Switches" H 6700 3250 60  0001 L CNN "Category"
F 7 "Pushbutton Switches" H 6700 3350 60  0001 L CNN "Family"
F 8 "http://spec_sheets.e-switch.com/specs/29-KS01Q01.pdf" H 6700 3450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/e-switch/KS-01Q-01/EG4791-ND/2116270" H 6700 3550 60  0001 L CNN "DK_Detail_Page"
F 10 "SWITCH PUSH SPST-NO 0.01A 35V" H 6700 3650 60  0001 L CNN "Description"
F 11 "E-Switch" H 6700 3750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6700 3850 60  0001 L CNN "Status"
	1    6500 2650
	1    0    0    -1  
$EndComp
Text Notes 5400 2400 0    60   ~ 0
Reset
Text Notes 6350 2400 0    60   ~ 0
Flash
$Comp
L Web-NeoPixel-Controller-rescue:R R1
U 1 1 5B3FDDB7
P 5750 3400
F 0 "R1" V 5830 3400 50  0000 C CNN
F 1 "10K" V 5750 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3400 50  0001 C CNN
F 3 "" H 5750 3400 50  0000 C CNN
	1    5750 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3400 5150 3400
Wire Wire Line
	5150 3400 5150 3250
Connection ~ 5150 3250
Wire Wire Line
	5900 3400 6000 3400
Wire Wire Line
	6700 4500 3950 4500
Wire Wire Line
	6000 3400 6000 2950
Wire Wire Line
	6000 2950 5250 2950
Wire Wire Line
	5250 2950 5250 2750
Wire Wire Line
	5250 2750 5350 2750
Connection ~ 6000 3400
Text Label 5750 2750 0    60   ~ 0
GND
$Comp
L Web-NeoPixel-Controller-rescue:R R2
U 1 1 5B3FE8F0
P 5750 3600
F 0 "R2" V 5830 3600 50  0000 C CNN
F 1 "10K" V 5750 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3600 50  0001 C CNN
F 3 "" H 5750 3600 50  0000 C CNN
	1    5750 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 3600 5900 3600
Wire Wire Line
	5600 3600 5100 3600
Wire Wire Line
	5100 3600 5100 3250
Connection ~ 5100 3250
Wire Wire Line
	6700 3100 6700 3200
$Comp
L Web-NeoPixel-Controller-rescue:C C3
U 1 1 5B3FECB0
P 6800 2950
F 0 "C3" H 6825 3050 50  0000 L CNN
F 1 "100nF" H 6825 2850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6838 2800 50  0001 C CNN
F 3 "" H 6800 2950 50  0000 C CNN
	1    6800 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 3100 6650 2950
Connection ~ 6650 3100
Text Label 6950 2950 0    60   ~ 0
GND
Connection ~ 3950 3650
Wire Wire Line
	3950 4500 3950 3650
$Comp
L Web-NeoPixel-Controller-rescue:R R5
U 1 1 5B3FF052
P 7350 3900
F 0 "R5" V 7430 3900 50  0000 C CNN
F 1 "10K" V 7350 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7280 3900 50  0001 C CNN
F 3 "" H 7350 3900 50  0000 C CNN
	1    7350 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 3900 7200 3900
$Comp
L Web-NeoPixel-Controller-rescue:R R3
U 1 1 5B3FF4B7
P 5750 3800
F 0 "R3" V 5830 3800 50  0000 C CNN
F 1 "10K" V 5750 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3800 50  0001 C CNN
F 3 "" H 5750 3800 50  0000 C CNN
	1    5750 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 3700 6100 3700
Wire Wire Line
	6100 3700 6100 3800
Wire Wire Line
	6100 3800 5900 3800
Wire Wire Line
	5600 3800 5050 3800
Wire Wire Line
	5050 3800 5050 3250
Connection ~ 5050 3250
$Comp
L Web-NeoPixel-Controller-rescue:R R4
U 1 1 5B3FF845
P 5750 4000
F 0 "R4" V 5830 4000 50  0000 C CNN
F 1 "10K" V 5750 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 4000 50  0001 C CNN
F 3 "" H 5750 4000 50  0000 C CNN
	1    5750 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 3800 6150 3800
Wire Wire Line
	6150 3800 6150 4000
Wire Wire Line
	6150 4000 6000 4000
Wire Wire Line
	5600 4000 5000 4000
Wire Wire Line
	5000 4000 5000 3250
Connection ~ 5000 3250
Text Label 6300 2750 2    60   ~ 0
IO0
Text Label 6700 2750 0    60   ~ 0
GND
Text Notes 4500 3650 0    60   ~ 0
Regulator
$Comp
L Web-NeoPixel-Controller-rescue:CONN_01X06 P2
U 1 1 5B401413
P 7650 2900
F 0 "P2" H 7650 3250 50  0000 C CNN
F 1 "CONN_01X06" V 7750 2900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 7650 2900 50  0001 C CNN
F 3 "" H 7650 2900 50  0000 C CNN
	1    7650 2900
	1    0    0    -1  
$EndComp
Text Notes 7400 2500 0    60   ~ 0
FTDI Connector
Text Label 7450 2650 2    60   ~ 0
GND
Text Label 7450 2750 2    60   ~ 0
CTS#
Text Label 4850 3250 0    60   ~ 0
VCC
Wire Wire Line
	7450 2950 7150 2950
Wire Wire Line
	7150 2950 7150 3600
Wire Wire Line
	7150 3600 7000 3600
Wire Wire Line
	7000 3700 7250 3700
Wire Wire Line
	7250 3700 7250 3050
Wire Wire Line
	7250 3050 7450 3050
Text Label 7450 3150 2    60   ~ 0
RTS#
NoConn ~ 7000 4000
NoConn ~ 7000 4100
NoConn ~ 6200 4100
NoConn ~ 6200 4000
NoConn ~ 6200 3500
NoConn ~ 5750 2550
NoConn ~ 5350 2550
NoConn ~ 6300 2550
NoConn ~ 6700 2550
NoConn ~ 7450 2750
NoConn ~ 7450 3150
Wire Wire Line
	4050 2850 4050 3250
Connection ~ 4050 2850
Wire Wire Line
	4050 3250 4150 3250
Connection ~ 4050 3250
Wire Wire Line
	7500 3900 7600 3900
Text Label 7600 3900 0    60   ~ 0
GND
Connection ~ 6000 4000
Wire Wire Line
	6000 4000 6000 4150
Text Label 6000 4150 0    60   ~ 0
IO0
Wire Wire Line
	4850 3350 4850 3250
Text Notes 4550 650  0    60   ~ 0
NeoPixel Strip
$Comp
L dk_Logic-Translators-Level-Shifters:SN74LVC1T45DBVR U2
U 1 1 5B4BB857
P 5350 1750
F 0 "U2" H 5200 2100 60  0000 C CNN
F 1 "SN74LVC1T45DBVR" H 5300 2200 60  0000 R CNN
F 2 "digikey-footprints:SOT-23-6" H 5550 1950 60  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1t45.pdf" H 5550 2050 60  0001 L CNN
F 4 "296-16843-1-ND" H 5550 2150 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1T45DBVR" H 5550 2250 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5550 2350 60  0001 L CNN "Category"
F 7 "Logic - Translators, Level Shifters" H 5550 2450 60  0001 L CNN "Family"
F 8 "http://www.ti.com/lit/ds/symlink/sn74lvc1t45.pdf" H 5550 2550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1T45DBVR/296-16843-1-ND/639459" H 5550 2650 60  0001 L CNN "DK_Detail_Page"
F 10 "IC TRNSLTR BIDIRECTIONAL SOT23-6" H 5550 2750 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 5550 2850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5550 2950 60  0001 L CNN "Status"
	1    5350 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2850 4700 1000
Text Label 5450 2150 0    60   ~ 0
GND
Text Label 5050 1750 2    60   ~ 0
DATA
Wire Wire Line
	4800 1000 4800 1100
Text Label 4800 1100 0    60   ~ 0
5V_DATA
Text Label 5650 1750 0    60   ~ 0
5V_DATA
$Comp
L Web-NeoPixel-Controller-rescue:C C4
U 1 1 5B4BE054
P 5900 1050
F 0 "C4" H 5925 1150 50  0000 L CNN
F 1 "100nF" H 5925 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5938 900 50  0001 C CNN
F 3 "" H 5900 1050 50  0000 C CNN
	1    5900 1050
	0    1    1    0   
$EndComp
$Comp
L Web-NeoPixel-Controller-rescue:C C5
U 1 1 5B4BE1AC
P 5900 1300
F 0 "C5" H 5925 1400 50  0000 L CNN
F 1 "100nF" H 5925 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5938 1150 50  0001 C CNN
F 3 "" H 5900 1300 50  0000 C CNN
	1    5900 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 1350 5350 1050
Wire Wire Line
	5350 1050 5500 1050
Wire Wire Line
	5450 1350 5450 1300
Wire Wire Line
	5450 1300 5550 1300
Text Label 6050 1050 0    60   ~ 0
GND
Text Label 6050 1300 0    60   ~ 0
GND
Text Notes 6100 1150 0    60   ~ 0
Bypass Capacitors
Wire Wire Line
	5500 1050 5500 900 
Connection ~ 5500 1050
Text Label 5500 900  0    60   ~ 0
VCC
Wire Wire Line
	5550 1300 5550 1250
Connection ~ 5550 1300
Text Label 5550 1250 0    60   ~ 0
+5V
Text Notes 5550 1900 0    60   ~ 0
Level Shifter
Text Label 5050 1950 2    60   ~ 0
VCC
$Comp
L PJ-037A:PJ-037A CON1
U 1 1 5B4D77B1
P 3450 2950
F 0 "CON1" H 3149 3150 50  0000 L BNN
F 1 "PJ-037A" H 3149 2750 50  0000 L BNN
F 2 "PJ-037A:CUI_PJ-037A" H 3450 2950 50  0001 L BNN
F 3 "Manufacturer recommendations" H 3450 2950 50  0001 L BNN
F 4 "CUI INC" H 3450 2950 50  0001 L BNN "Field4"
	1    3450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2850 4050 2850
Wire Wire Line
	3900 3050 3650 3050
Text Label 3650 2850 0    60   ~ 0
+5V
Text Label 3650 3050 0    60   ~ 0
GND
Wire Wire Line
	4050 3650 4250 3650
Wire Wire Line
	4950 3250 5000 3250
Wire Wire Line
	4250 3650 4450 3650
Wire Wire Line
	5150 3250 5250 3250
Wire Wire Line
	6000 3400 6200 3400
Wire Wire Line
	5100 3250 5150 3250
Wire Wire Line
	6650 3100 6700 3100
Wire Wire Line
	3950 3650 4050 3650
Wire Wire Line
	5050 3250 5100 3250
Wire Wire Line
	5000 3250 5050 3250
Wire Wire Line
	4050 2850 4700 2850
Wire Wire Line
	4050 3250 4050 3300
Wire Wire Line
	6000 4000 5900 4000
Wire Wire Line
	5500 1050 5750 1050
Wire Wire Line
	5550 1300 5750 1300
Text Label 7450 2850 2    50   ~ 0
FTDI_5V
NoConn ~ 7000 3800
Text Label 6200 3900 2    50   ~ 0
DATA
$Comp
L Diode:MBR0520LT D1
U 1 1 5B6F8D15
P 7200 2150
F 0 "D1" H 7200 2366 50  0000 C CNN
F 1 "MBR0520LT" H 7200 2275 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7200 1975 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MBR0520LT1-D.PDF" H 7200 2150 50  0001 C CNN
	1    7200 2150
	1    0    0    -1  
$EndComp
Text Label 7350 2150 0    50   ~ 0
FTDI_5V
Text Label 7050 2150 2    50   ~ 0
+5V
$EndSCHEMATC
