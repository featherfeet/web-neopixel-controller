#ifndef SHOWIPADDRESS_HPP
#define SHOWIPADDRESS_HPP

#include <ESP8266WiFi.h>
#include <FastLED.h>

void showIPAddress(CRGB* leds, int num_leds) {
  fill_solid(leds, num_leds, CRGB(0, 0, 0));
	IPAddress ip = WiFi.localIP();
  uint8_t ip_bytes[4] = {ip[0], ip[1], ip[2], ip[3]};
  if (ip_bytes[0] == 0 && ip_bytes[1] == 0 && ip_bytes[2] == 0 && ip_bytes[3] == 0) {
    ip = WiFi.softAPIP();
    ip_bytes[0] = ip[0];
    ip_bytes[1] = ip[1];
    ip_bytes[2] = ip[2];
    ip_bytes[3] = ip[3];
  }
	uint8_t led_i = 0;
	for (uint8_t ip_i = 0; ip_i < 4; ip_i++) {
		uint8_t ip_byte = ip_bytes[ip_i];
		for (int8_t bit_i = 7; bit_i >= 0; bit_i--) {
			bool bit = bitRead(ip_byte, bit_i);
			if (bit == 0) {
				leds[led_i] = CRGB(0, 0, 255);
			}
			else {
				leds[led_i] = CRGB(255, 0, 0);
			}
			led_i++;
		}
		led_i += 3;
	}
}

#endif // SHOWIPADDRESS_HPP
