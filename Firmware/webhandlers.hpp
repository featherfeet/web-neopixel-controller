#ifndef WEBHANDLERS_HPP
#define WEBHANDLERS_HPP

#include "webpages.hpp"

#define SERVER_TYPE_HTTP 0
#define SERVER_TYPE_HTTPS 1

uint8_t server_type = SERVER_TYPE_HTTP; // The main program changes this variable before each request handler is called so that the handlers know which of the server objects to get the URI from.

ESP8266WebServer serverHTTP(80);
BearSSL::ESP8266WebServerSecure serverHTTPS(443);

String getServerURI() {
  if (server_type == SERVER_TYPE_HTTP) {
    return serverHTTP.uri();
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    return serverHTTPS.uri();
  }
  return "ERROR";
}

void serverSendHTML(uint16_t http_code, const char *html_content) {
  if (server_type == SERVER_TYPE_HTTP) {
    serverHTTP.send(http_code, "text/html", html_content);
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    serverHTTPS.send(http_code, "text/html", html_content);
  }
}

void serverSendHTML(uint16_t http_code, const __FlashStringHelper *html_content) {
  if (server_type == SERVER_TYPE_HTTP) {
    serverHTTP.send(http_code, "text/html", html_content);
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    serverHTTPS.send(http_code, "text/html", html_content);
  }
}

void serverSendPlain(uint16_t http_code, const char *text_content) {
  if (server_type == SERVER_TYPE_HTTP) {
    serverHTTP.send(http_code, "text/plain", text_content);
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    serverHTTPS.send(http_code, "text/plain", text_content);
  }
}

void serverSendPlain(uint16_t http_code, const __FlashStringHelper *text_content) {
  if (server_type == SERVER_TYPE_HTTP) {
    serverHTTP.send(http_code, "text/plain", text_content);
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    serverHTTPS.send(http_code, "text/plain", text_content);
  }
}

HTTPMethod getServerMethod() {
  if (server_type == SERVER_TYPE_HTTP) {
    return serverHTTP.method();
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    return serverHTTPS.method();
  }
  return HTTP_GET;
}

String getServerFormArgument(const char *argument_name) {
  if (server_type == SERVER_TYPE_HTTP) {
    return serverHTTP.arg(argument_name);
  }
  else if (server_type == SERVER_TYPE_HTTPS) {
    return serverHTTPS.arg(argument_name);
  }
  return "";
}

void handleRoot() {
  Serial.print("Client asking for: ");
  Serial.println(getServerURI());
  serverSendHTML(200, index_html);
}

void handleSetup() {
  Serial.print("Client asking for: ");
  Serial.println(getServerURI());
  HTTPMethod method = getServerMethod();
  if (method == HTTP_GET) {
    serverSendHTML(200, setup_html);
  }
  else if (method == HTTP_POST) {
    strcpy(setup_data.SSID, getServerFormArgument("ssid").c_str());
    strcpy(setup_data.PSK, getServerFormArgument("password").c_str());
    setup_data.leds = getServerFormArgument("leds").toInt();
    EEPROM.put(0, setup_data);
    EEPROM.put(sizeof(setup_data), SETUP_DATA_STATUS_INITIALIZED);
    EEPROM.commit();
    serverSendHTML(200, PSTR("<html><head><meta charset='utf-8' lang='en' /></head><body><p>Configuration saved to EEPROM. ESP will now reboot and connect to your Wi-Fi.</p></body></html>"));
    delay(500);
    ESP.restart();
  }
}

void handleOtherPageRequest() {
  String url = getServerURI();
  Serial.print("Client asking for: ");
  Serial.println(url);
  if (url.equals("/all-red")) {
    fill_solid(leds,setup_data.leds,CRGB(255,0,0));
    ANIMATION_MODE=NULL;
    FastLED.show();
    serverSendPlain(200, "ALL RED");
    currentColor=CHSV(0,255,255);
  }
  else if (url.equals("/all-green")) {
    fill_solid(leds,setup_data.leds,CRGB(0,255,0));
    ANIMATION_MODE=NULL;
    FastLED.show();
    serverSendPlain(200, "ALL GREEN");
    currentColor=CHSV(85,255,250);
  }
  else if (url.equals("/all-blue")) {
    fill_solid(leds,setup_data.leds,CRGB(0,0,255));
    ANIMATION_MODE=NULL;
    FastLED.show();
    serverSendPlain(200, "ALL BLUE");
    currentColor=CHSV(170,255,255);
  }
  else if (url.equals("/all-white")) {
    fill_solid(leds,setup_data.leds,CRGB(255,255,255));
    ANIMATION_MODE=NULL;
    FastLED.show();
    serverSendPlain(200, "ALL WHITE");
    currentColor=CHSV(0,0,255);
  }
  else if (url.equals("/all-off")) {
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    ANIMATION_MODE=NULL;
    FastLED.show();
    serverSendPlain(200, "ALL OFF");
    currentColor=CHSV(0,0,0);
  }
  else if (url.indexOf("/hsvcolor-")!=-1) {
    ANIMATION_MODE=NULL;
    String h;
    String s;
    String v;
    int i;
    int currentNum=0;
    url.remove(0,10);
    Serial.print("URL after truncation: ");
    Serial.println(url);
    for (i=0;i<url.length();i++)
    {
      Serial.print("i: ");
      Serial.println(i);
      Serial.print("charAt(i): ");
      Serial.println(url.charAt(i));
      if (url.charAt(i)=='-')
      {
        currentNum++;
      }
      else
      {
        if (currentNum==0)
        {
          h+=url.charAt(i);
        }
        else if (currentNum==1)
        {
          s+=url.charAt(i);
        }
        else if (currentNum==2)
        {
          v+=url.charAt(i);
        }
      }
    }
    Serial.print("h: ");
    Serial.println(h);
    Serial.print("s: ");
    Serial.println(s);
    Serial.print("v: ");
    Serial.println(v);
    currentColor.h=h.toInt();
    currentColor.s=s.toInt();
    currentColor.v=v.toInt();
    fill_solid(leds,setup_data.leds,CHSV(h.toInt(),s.toInt(),v.toInt()));
    FastLED.show();
    String hsv_color_string = "HSV COLOR: ("+h+","+s+","+v+")";
    serverSendPlain(200, hsv_color_string.c_str());
  }
  else if (url.equals("/rainbow-dot")) {
    ANIMATION_MODE=ANIMATION_MODE_RAINBOW_DOT;
    serverSendPlain(200, "RAINBOW DOT");
  }
  else if (url.equals("/reversing-spectrum")) {
    ANIMATION_MODE=ANIMATION_MODE_REVERSING_SPECTRUM;
    serverSendPlain(200, "REVERSING SPECTRUM");
  }
  else if (url.equals("/oscillator-shields-are-down")) {
    ANIMATION_MODE=ANIMATION_MODE_OSCILLATOR_SHIELDS_ARE_DOWN;
    serverSendPlain(200, "OSCILLATOR SHIELDS ARE DOWN");
  }
  else if (url.equals("/aimless-crap")) {
    ANIMATION_MODE=ANIMATION_MODE_AIMLESS_CRAP;
    serverSendPlain(200, "AIMLESS CRAP");
  }
  else if (url.equals("/light-olivers-desk")) {
    ANIMATION_MODE=NULL;
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    int i;
    for (i=0;i<setup_data.leds/5;i++) {
      leds[i]=currentColor;
    }
    FastLED.show();
    serverSendPlain(200, "LIGHT OLIVERS DESK");
  }
  else if (url.equals("/light-olivers-bed")) {
    ANIMATION_MODE=NULL;
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    int i;
    for (i=setup_data.leds/5;i<setup_data.leds;i++) {
      leds[i]=currentColor;
    }
    FastLED.show();
    serverSendPlain(200, "LIGHT OLIVERS BED");
  }
  else if (url.equals("/tardis-in-transit")) {
    ANIMATION_MODE=ANIMATION_MODE_TARDIS_IN_TRANSIT;
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    int streak_length=50;
    int distance_between_streaks=10;
    int brightness_step_between_pixels=255/streak_length;
    int i=0;
    int brightness=1;
    while (i<setup_data.leds) {
      leds[i]=CHSV(currentColor.h,currentColor.s,brightness);
      i++;
      brightness+=brightness_step_between_pixels;
      if (brightness>=255) {
        brightness=1;
        i+=distance_between_streaks;
      }
    }
    FastLED.show();
    serverSendPlain(200, "TARDIS IN TRANSIT");
  }
  else if (url.equals("/pause")) {
    ANIMATION_MODE=NULL;
    serverSendPlain(200, "PAUSE");
  }
  else if (url.equals("/save")) {
    int i;
    ANIMATION_MODE=NULL;
    for (i=0;i<setup_data.leds;i++)
    {
      saved_state[i]=leds[i];
    }
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    FastLED.show();
    serverSendPlain(200, "SAVE");
  }
  else if (url.equals("/restore"))
  {
    int i;
    ANIMATION_MODE=NULL;
    for (i=0;i<setup_data.leds;i++)
    {
      leds[i]=saved_state[i];
    }
    FastLED.show();
    serverSendPlain(200, "RESTORE");
  }
  else if (url.equals("/scroll-forward"))
  {
    ANIMATION_MODE=ANIMATION_MODE_SCROLL_FORWARD;
    serverSendPlain(200, "SCROLL FORWARD");
  }
  else if (url.equals("/scroll-backward"))
  {
    ANIMATION_MODE=ANIMATION_MODE_SCROLL_BACKWARD;
    serverSendPlain(200, "SCROLL BACKWARD");
  }
  else {
    Serial.println(url);
    serverSendPlain(404, FPSTR(pagenotfound_html));
  }
}

#endif // WEBHANDLERS_HPP
