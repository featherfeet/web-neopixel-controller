#!/bin/bash

# Empty out the header file.
rm ssl_data.hpp
touch ssl_data.hpp

# Generate a self-signed SSL key and certificate.
openssl req -x509 -newkey rsa:512 -sha256 -keyout serverKey -out serverCertificate -days 398 -nodes -subj "/C=US/ST=California/L=Pleasanton/O=Blinkstream [US]/OU=Blinkstream/CN=blinkstream" -addext subjectAltName=DNS:blinkstream

# Add the start of the include guard to the header.
echo "#ifndef SSL_DATA_HPP" >> ssl_data.hpp
echo "#define SSL_DATA_HPP" >> ssl_data.hpp
echo "" >> ssl_data.hpp

# Use xxd to generate arrays of hex values as C-strings that encode the SSL key and certificate.
xxd -i serverKey >> ssl_data.hpp
echo "" >> ssl_data.hpp
xxd -i serverCertificate >> ssl_data.hpp

# Delete the key and certificate files.
# rm serverKey serverCertificate

# Delete the useless "len" variables that xxd adds to specify array lengths.
gawk -i inplace '!/_len = /' ssl_data.hpp

# Add null termination bytes to the ends of the arrays.
sed -i 's/\([0-9a-f]\)$/\0, 0x00/g' ssl_data.hpp

# Change the arrays type declarations to make the compiler store them in program memory as constants.
sed -i 's/unsigned char/static const char/g' ssl_data.hpp
sed -i 's/\[\]/\[\] PROGMEM/g' ssl_data.hpp

# Add the end of the include guard to the header.
echo "" >> ssl_data.hpp
echo "#endif // SSL_DATA_HPP" >> ssl_data.hpp
