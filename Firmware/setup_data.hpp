#ifndef SETUP_DATA_HPP
#define SETUP_DATA_HPP

struct SetupData {
  char SSID[32];
  char PSK[32];
  int leds;
};
SetupData setup_data;
uint8_t setup_data_status = 0;

#endif // SETUP_DATA_HPP
