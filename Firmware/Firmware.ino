#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WebServerSecure.h>
#include <ESP8266mDNS.h>
#include <ESP8266Ping.h> // from https://github.com/dancol90/ESP8266Ping

#define FASTLED_ESP8266_RAW_PIN_ORDER
#define FASTLED_ALLOW_INTERRUPTS 0
#include <FastLED.h>

#include <EEPROM.h>

#include <string.h>
#include <stdio.h>

#include "webpages.hpp"
#include "showipaddress.hpp"
#include "ssl_data.hpp"
#include "definitions.hpp"
#include "setup_data.hpp"
#include "leds.hpp"
#include "webhandlers.hpp"

//#define FRAME_DELAY() delay(20)
#define FRAME_DELAY() //

// The ICACHE_RAM_ATTR macro is necessary on all ISRs to put them in IRAM (otherwise the ESP8266 crashes and goes into a reboot loop.
void ICACHE_RAM_ATTR handleIPButtonPress() {
	ANIMATION_MODE = ANIMATION_MODE_IP_ADDRESS;
}

IPAddress gateway;
unsigned long last_ping_time = 0;

void setup() {
  // put your setup code here, to run once:
  // Initialize the RNG using the time since boot.
  randomSeed(millis());
  // Set up the serial with the same baud rate as the ESP8266 bootup messages.
  Serial.begin(74880);
  Serial.println("Begun serial.");
  // Read settings from EEPROM.
  EEPROM.begin(sizeof(SetupData) + sizeof(uint8_t));
  EEPROM.get(0, setup_data);
  EEPROM.get(sizeof(setup_data), setup_data_status);
  // Set up an interrupt on the FLASH button on GPIO 0 that shows the IP address in binary on the LED strip.
  attachInterrupt(digitalPinToInterrupt(IP_ADDRESS_SHOW_PIN), handleIPButtonPress, RISING);
  // If the settings are not already set, then set up an access point (AP) to allow first-time configuration.
  if (setup_data_status != SETUP_DATA_STATUS_INITIALIZED) {
    // Setup data has not been initialized.
    WiFi.mode(WIFI_AP);
    WiFi.hostname("blinkstream");
    WiFi.softAP("blinkstream");
    WiFi.begin();
    if (MDNS.begin("blinkstream")) {
      Serial.println("mDNS responder started.");
      MDNS.addService("http", "tcp", 80);
    }
    // Set a default number of LEDs.
    setup_data.leds = 300;
    // Allocate memory for a buffer storing the LEDs' colors.
    leds = (CRGB*) malloc(setup_data.leds * sizeof(CRGB));
    // Allocate memory to allow save/restore of the LEDs' states.
    saved_state = (CRGB*) malloc(setup_data.leds * sizeof(CRGB));
    // Initialize the FastLED library.
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, setup_data.leds);
    // Black out the LEDs and display that.
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    FastLED.show();
    Serial.println("Begun NeoPixels.");
    Serial.println("Setup data has not been initialized... Connect to the ESP's access point and go to http://192.168.4.1/setup to configure the ESP.");
  }
  // If the settings have already been set, then load them and connect to Wi-Fi.
  else {
    Serial.println("Loaded configuration from EEPROM...");
    Serial.print("SSID: ");
    Serial.println(setup_data.SSID);
    Serial.print("PSK: ");
    Serial.println(setup_data.PSK);
    Serial.print("LEDs: ");
    Serial.println(setup_data.leds);
    WiFi.mode(WIFI_STA);
    WiFi.hostname("blinkstream");
    WiFi.begin(setup_data.SSID, setup_data.PSK);
    Serial.println("Begun wifi.");
    if (MDNS.begin("blinkstream")) {
      Serial.println("mDNS responder started.");
      MDNS.addService("http", "tcp", 80);
    }
    // Allocate memory for a buffer storing the LEDs' colors.
    leds = (CRGB*) malloc(setup_data.leds * sizeof(CRGB));
    // Allocate memory to allow save/restore of the LEDs' states.
    saved_state = (CRGB*) malloc(setup_data.leds * sizeof(CRGB));
    // Initialize the FastLED library.
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, setup_data.leds);
    // Black out the LEDs and display that.
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    FastLED.show();
    Serial.println("Begun NeoPixels.");
    // Connect to Wi-Fi, showing a green progress bar on the LED strip until connected.
    int i = 0;
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.println("Connecting...");
      leds[i]=CRGB(0,255,0);
      if (i<setup_data.leds) {
        i++;
      }
      FastLED.show();
    }
    Serial.println("Connected...");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    gateway = WiFi.gatewayIP();
    Serial.println("Gateway at: " + gateway.toString());
    // Black out the LEDs and display that.
    fill_solid(leds,setup_data.leds,CRGB(0,0,0));
    FastLED.show();
  }
  // Set up the HTTP server.
  serverHTTP.on("/",handleRoot);
  serverHTTP.on("/setup", handleSetup);
  serverHTTP.onNotFound(handleOtherPageRequest);
  // Set up HTTPS server.
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  serverHTTPS.getServer().setRSACert(new BearSSL::X509List(serverCertificate), new BearSSL::PrivateKey(serverKey));
  serverHTTPS.on("/",handleRoot);
  serverHTTPS.on("/setup", handleSetup);
  serverHTTPS.onNotFound(handleOtherPageRequest);
  // Start the web servers.
  Serial.println("Servers starting...");
  serverHTTP.begin();
  serverHTTPS.begin();
  Serial.println("Servers started.");
}

void loop() {
  // put your main code here, to run repeatedly:
  // If the Wi-Fi settings are initialized but the Wi-Fi has disconnected, then pulsate the LEDs red.
  if (setup_data_status == SETUP_DATA_STATUS_INITIALIZED && WiFi.status() != WL_CONNECTED) {
    fill_solid(leds,setup_data.leds,CRGB(255,0,0));
    ANIMATION_MODE = ANIMATION_MODE_OSCILLATOR_SHIELDS_ARE_DOWN;
    FastLED.show();
    currentColor=CHSV(0,255,255);
  }
  // Ping the gateway every 3 minutes.
  if (millis() - last_ping_time > 180000) {
    Serial.println("Pinging gateway at " + gateway.toString());
    if(Ping.ping(gateway)) {
      Serial.println("Ping successful.");
    }
    last_ping_time = millis();
  }
  // Serve NeoPixel controller web pages and run handlers for RESTful API requests.
  server_type = SERVER_TYPE_HTTP;
  serverHTTP.handleClient();
  server_type = SERVER_TYPE_HTTPS;
  serverHTTPS.handleClient();
  MDNS.update();
  // Show one frame of whichever animation is currently being shown.
  if (ANIMATION_MODE==ANIMATION_MODE_RAINBOW_DOT) {
    leds[lit_led-lit_led_delta]=CRGB(0,0,0);
    leds[lit_led]=CHSV(lit_led_color,255,255);
    if (lit_led+lit_led_delta>setup_data.leds || lit_led+lit_led_delta<0) {
      lit_led_delta=-lit_led_delta;
    }
    if (lit_led_color+lit_led_color_delta>255 || lit_led_color+lit_led_color_delta<0) {
      lit_led_color_delta=-lit_led_color_delta;
    }
    FastLED.show();
    lit_led+=lit_led_delta;
    lit_led_color+=lit_led_color_delta;
  }
  else if (ANIMATION_MODE==ANIMATION_MODE_REVERSING_SPECTRUM) {
    leds[lit_led]=CHSV(lit_led_color,255,255);
    if (lit_led+lit_led_delta>setup_data.leds || lit_led+lit_led_delta<0) {
      lit_led_delta=-lit_led_delta;
    }
    if (lit_led_color+lit_led_color_delta>255 || lit_led_color+lit_led_color_delta<0) {
      lit_led_color_delta=-lit_led_color_delta;
    }
    lit_led+=lit_led_delta;
    lit_led_color+=lit_led_color_delta;
    FastLED.show();
  }
  else if (ANIMATION_MODE==ANIMATION_MODE_OSCILLATOR_SHIELDS_ARE_DOWN) {
    if ((brightness+brightness_delta)>255 || (brightness+brightness_delta)<0) {
      brightness_delta=-brightness_delta;
    }
    brightness+=brightness_delta;
    fill_solid(leds,setup_data.leds,CHSV(currentColor.h,currentColor.s,brightness));
    FastLED.show();
  }
  else if (ANIMATION_MODE==ANIMATION_MODE_AIMLESS_CRAP) {
    int i;
    for (i=0;i<setup_data.leds;i++) {
      leds[i]=CHSV(random(0,256),255,255);
    }
    FastLED.show();
    FRAME_DELAY();
  }
  else if (ANIMATION_MODE==ANIMATION_MODE_TARDIS_IN_TRANSIT || ANIMATION_MODE==ANIMATION_MODE_SCROLL_FORWARD) {
    CRGB endcolor=leds[setup_data.leds-1];
    int i;
    for (i=setup_data.leds-1;i>-1;i--) {
      leds[i]=leds[i-1];
    }
    leds[0]=endcolor;
    FastLED.show();
    FRAME_DELAY();
  }
  else if (ANIMATION_MODE==ANIMATION_MODE_SCROLL_BACKWARD) {
    CRGB startcolor=leds[0];
    int i;
    for (i=0;i<setup_data.leds-1;i++) {
      leds[i]=leds[i+1];
    }
    leds[setup_data.leds-1]=startcolor;
    FastLED.show();
    FRAME_DELAY();
  }
  else if (ANIMATION_MODE == ANIMATION_MODE_IP_ADDRESS) {
    showIPAddress(leds, setup_data.leds);
    FastLED.show();
    FRAME_DELAY();
  }
}
