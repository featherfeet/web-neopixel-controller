#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2017-01-14 12:44:26

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "libraries/FastLED/FastLED.h"
void handleRoot() ;
void handleOtherPageRequest() ;
void setup() ;
void loop() ;

#include "WebServer_FastLED.ino"


#endif
