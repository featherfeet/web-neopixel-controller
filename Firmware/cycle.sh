#!/bin/bash

BLINKSTREAM_IP="192.168.1.108"
DELAY_TIME_SECONDS=60

while :
do
    # All red.
    curl "$BLINKSTREAM_IP/all-red"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # All cyan (looks cool and icy on NeoPixels).
    curl "$BLINKSTREAM_IP/hsvcolor-128-255-255"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # All yellow.
    curl "$BLINKSTREAM_IP/hsvcolor-43-255-255"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Color-changing dot bounces from end to end.
    curl "$BLINKSTREAM_IP/rainbow-dot"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Color-changing dot becomes a rainbow that bounces around.
    curl "$BLINKSTREAM_IP/reversing-spectrum"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # All blue.
    curl "$BLINKSTREAM_IP/all-blue"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Pulsate blue.
    curl "$BLINKSTREAM_IP/oscillator-shields-are-down"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Completely random colors.
    curl "$BLINKSTREAM_IP/aimless-crap"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # All green.
    curl "$BLINKSTREAM_IP/all-green"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Snake-looking patterns (in green).
    curl "$BLINKSTREAM_IP/tardis-in-transit"
    echo ""
    sleep $DELAY_TIME_SECONDS

    # Freeze the random colors and scroll them forward on the strip to make a sort of "carnival lights" effect.
    curl "$BLINKSTREAM_IP/aimless-crap"
    curl "$BLINKSTREAM_IP/pause"
    curl "$BLINKSTREAM_IP/scroll-forward"
    sleep $DELAY_TIME_SECONDS
    echo ""
done
