#!/bin/bash

# Empty out the header file.
rm webpages.hpp
touch webpages.hpp

# Add the start of the include guard to the header.
echo "#ifndef WEBPAGES_HPP" >> webpages.hpp
echo "#define WEBPAGES_HPP" >> webpages.hpp
echo "" >> webpages.hpp

# Use xxd to generate arrays of hex values as C-strings that encode the HTML files.
xxd -i index.html >> webpages.hpp
echo "" >> webpages.hpp
xxd -i pagenotfound.html >> webpages.hpp
echo "" >> webpages.hpp
xxd -i setup.html >> webpages.hpp

# Delete the useless "len" variables that xxd adds to specify array lengths.
gawk -i inplace '!/_len = /' webpages.hpp

# Add null termination bytes to the ends of the arrays.
sed -i 's/\([0-9a-f]\)$/\0, 0x00/g' webpages.hpp

# Change the arrays type declarations to make the compiler store them in program memory as constants.
sed -i 's/unsigned char/static const char/g' webpages.hpp
sed -i 's/\[\]/\[\] PROGMEM/g' webpages.hpp

# Add the end of the include guard to the header.
echo "" >> webpages.hpp
echo "#endif // WEBPAGES_HPP" >> webpages.hpp
