#ifndef LEDS_HPP
#define LEDS_HPP

CRGB *leds;
CRGB *saved_state;
volatile int ANIMATION_MODE = NULL;
int lit_led = 0;
int lit_led_color = 0;
int lit_led_color_delta = 1;
int lit_led_delta = 1;
int brightness = 0;
int brightness_delta = 3;
CHSV currentColor = CHSV(255,0,255);

#endif // LEDS_HPP
